﻿using System;

namespace TaxApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("Witaj w aplikacji obliczającej pensję netto!");
            Console.WriteLine("Podaj miesięczną pensję brutto:");
            var allowance = Console.ReadLine();

            Console.WriteLine("Wybierz rodzaj umowy:");
            foreach (var type in Enum.GetValues(typeof(ContractType)))
                Console.WriteLine($"- {type}");
            var contractType = Console.ReadLine();

            var taxCalculator = new TaxCalculator();
            var netAllowance = taxCalculator.CalculateTax((ContractType)Enum.Parse(typeof(ContractType), contractType), int.Parse(allowance));
            Console.WriteLine($"Twoje zarobki to: {netAllowance}");
        }
    }
}
