﻿using System;

namespace TaxApp
{
    public enum ContractType
    {
        B2B,
        EmploymentContract
    }
    public class TaxCalculator
    {
        public double CalculateTax(ContractType contractType, int grossAllowance)
        {
            if (contractType == ContractType.B2B)
            {
                return grossAllowance*0.81;
            }

            if (contractType == ContractType.EmploymentContract)
            {
                return grossAllowance*0.82;
            }

            throw new NotSupportedException("This contract type is not supported");
        }
    }
}
